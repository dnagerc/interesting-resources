# Interesting Resources
Here are some of the resources which i like to use for my productivity.

## MEMORY - Learn languages, vocabulary, homework; study for exams with flashcards & more
https://www.memory.com/

## Make Diagrams and More
https://whimsical.com/

## Typical Pomodoro Timer
https://pomodorotimer.online/es/


## Summary of Books
### Clean Code
#### Chapter 9: Unit Tests
https://whimsical.com/testing-Gd2qqBEDk2aWkW1NFFN9oP
#### Chapter 10: Classes
